function clr()
{
    document.getElementById("result").value = ""
}

function display(val)
{
    document.getElementById("result").value += val;
}

// dont use eval func as it posess security risks
function solve()
{
    let x = document.getElementById("result").value;
    let y = eval(x);
    document.getElementById("result").value = y;
}